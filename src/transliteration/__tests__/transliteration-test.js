import transliteration from '../../transliteration';


describe('transliteration function', () => {

    it("transliterates correctly special characters of uzbek ltn", () => {
        expect(transliteration('ltn', 'O‘zbek')).toEqual('Ўзбек');
    });

    it("transliterates correctly special characters of uzbek ltn", () => {
        expect(transliteration('crl', 'Ўзбек')).toEqual('O‘zbek');
    });

    it("transliterates correctly the names of the months", () => {
        expect(transliteration('crl', 'Ноябрь')).toEqual('Noyabr');
    });

    it("transliterates correctly the names of the months", () => {
        expect(transliteration('ltn', 'Noyabr')).toEqual('Ноябрь');
    });

    it("transliterates correctly the names of the months", () => {
        expect(transliteration('ltn', 'Oktabr')).toEqual('Октябрь');
    });

    it("transliterates correctly the names of the months", () => {
        expect(transliteration('crl', 'Октябрь')).toEqual('Oktabr');
    });

    it("transliterates correctly the urls", () => {
        expect(transliteration('ltn', 'www.google.com')).toEqual('www.google.com');
    });

    it("transliterates correctly the names of the months", () => {
        expect(transliteration('crl', 'Цирк')).toEqual('Sirk');
    });

});
