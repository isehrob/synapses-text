/**
 * In every object or array, lines corresponds to each other.
 *
 *
 * */
// for transliteration
// transliteration rules for crl to ltn
const exceptionalPhonemasCrl = [
	[
		// upper
		// add entries properly
		/([аоуеёияюўАОУЕЁИЯЮЎ])Е/g, /([аоуеёияюўАОУЕЁИЯЮЎ])Ц/g, /(^|[\s"«])Э/g, /(^|[\s"«])Е/g, /(^|[\s"«])Ш/g,
		/(\w)Ш/g, /Ь($|\s)/g, /(^|[\s"«])Я/g, /(^|[\s"«])Ю/g, /(^|[\s"«])Ё/g, /(^|[\s"«])Ч/g, /«|»/g,
		// lower
		/([аоуеёияюўАОУЕЁИЯЮЎ])е/g, /([аоуеёияюўАОУЕЁИЯЮЎ])ц/g, /(^|[\s"«])э/g, /(^|[\s"«])е/g, /ь($|[\s"»])/g,
		/(^|[\s"«])я/g, /(^|[\s"«])ю/g, /(^|[\s"«])ё/g, /(^|[\s"«])ч/g,

	],
	[
		// for upper
		"$1YE", "$1TS", "$1E", "$1Ye", "$1Sh", "$1SH", "$1", "$1Ya", "$1Yu", "$1Yo", "$1Ch", "\"",
		// for lower
		"$1ye", "$1ts", "$1e", "$1ye", "$1", "$1ya", "$1yu", "$1yo", "$1ch",
	]

];

// transliteration rules for ltn to crl
const exceptionalPhonemasLtn = [
	[
		// upper
		// append new entries accordingly
		/(^|[\s"])ye/g, /([aouei`'‘’])ye/g, /(^|[\s"«])e/g, /yo[`'‘’]/g, /yo/g, /yu/g, /ya/g, /sh/g, /ch/g, /g[`'‘’]/g,
		/o[`'‘’]/g, /([aouei`'‘’])ts/g, /(^|\s+)"/g, /"($|[\s,.?!-]+)/g,
		// lower
		/(^|[\s"])Ye/g, /(^|[\s"«])E/g, /Yo[`'‘’]/g, /Yo/g, /Yu/g, /Ya/g, /Sh/g, /Ch/g, /G[`'‘]/g, /O[`'‘’]/g,
		// another list
		/(^|[\s"«])YE/g, /([AOUEI`'‘’])YE/g, /YO[`'‘’]/g, /YO/g, /YU/g, /YA/g, /SH/g, /CH/g, /([AOUEI`'‘’])TS/g,
	],
	[
		// for upper
		// add entries properly
		"$1е", "$1е", "$1э", "йў", "ё", "ю", "я", "ш", "ч", "ғ", "ў", "$1ц", "$1«", "»$1",
		// for lower
		"$1Е", "$1Э", "Йў", "Ё", "Ю", "Я", "Ш", "Ч", "Ғ", "Ў",
		// another list
		"$1Е", "$1Е", "ЙЎ", "Ё", "Ю", "Я", "Ш", "Ч", "$1Ц",
	]
];

// max https://www.coco.ru/hello/&lsdjf_ min coco.ru
const urlPattern = /((?:https?:\/{2})?(?:\w+\.)?[\w0-9_-]+\.[\w]+\/?(?:\/?[\w0-9$#&_.;%-]\/?)*)/g;
// to keep the word from transliterating
const forceKeepPattern = /(?:^|[\s"«(])%[\w"«»0-9$#_.;,?!()-]+%(?:$|[\s"»)])/g;
// to force transliterate the word
const forceTranslitPattern = /(?:^|[\s"«(])#[\w"«»0-9$#_.;,?!()-]+#(?:$|[\s"»)])/g;


// alphabet for transliteration

var crlAlphabet = [
	'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у',
	'ф','х','ц','ч','ш','ы','ъ','ь','ъ','ь','э','ю','я','ў','қ','ғ','ҳ'
];

var ltnAlphabet = [
	'a','b','v','g','d','e','yo','j', 'z','i','y','k','l','m','n','o','p','r','s','t','u',
	'f','x','s','ch','sh','i','’','’','\'','\'','e','yu','ya','o‘','q','g‘','h'
];

// exceptional words crl
// every element at any index of the first exceptional words array corresponds to the element
// of the second exceptional words arrays at that index
const exceptionalWordsCrl = [

	// first
	/Октябрь/g, /Сентябрь/g, /Ноябрь/g, /Декабрь/g,	/Январь/g,	/Февраль/g, /Апрель/g, /Июнь/g,
	/Июль/g, /Бюджет/g, /Режиссёр/g, /Исҳоқ/g, /Нуқтаий/g, /Цезий/g, /Цейтнот/g, /Целлофан/g,
	/Целлюлоза/g, /Цельсий/g, /Цемент/g, /Цензор/g, /Цензура/g, /Центнер/g, /Цех/g, /Цеце/g,
	/Цивилизация/g, /Цикл/g, /Циклон/g, /Циклоп/g, /Цилиндр/g, /Цилиндрик/g, /Цилиндрсимон/g,
	/Цирк/g, /Циркуль/g, /Цистерна/g, /Цитрус/g, /Нью-Йорк/g, /Компьютер/g, /Ансамбль/g,
	/Аппелляция/g, /Апельсин/g, /Бандероль/g, /Бильярд/g, /Бульдозер/g, /Франция/g, /Экспорт/g, /Аэропорт/g,
	/Вьетнам/g, /Конференция/g,

	// second
	/ОКТЯБРЬ/g, /СЕНТЯБРЬ/g, /НОЯБРЬ/g, /ДЕКАБРЬ/g,	/ЯНВАРЬ/g,	/ФЕВРАЛЬ/g, /АПРЕЛЬ/g, /ИЮНЬ/g,
	/ИЮЛЬ/g, /БЮДЖЕТ/g, /РЕЖИССЁР/g, /ИСҲОҚ/g, /НУҚТАИЙ/g, /ЦЕЗИЙ/g, /ЦЕЙТНОТ/g, /ЦЕЛЛОФАН/g,
	/ЦЕЛЛЮЛОЗА/g, /ЦЕЛЬСИЙ/g, /ЦЕМЕНТ/g, /ЦЕНЗОР/g, /ЦЕНЗУРА/g, /ЦЕНТНЕР/g, /ЦЕХ/g, /ЦЕЦЕ/g,
	/ЦИВИЛИЗАЦИЯ/g, /ЦИКЛ/g, /ЦИКЛОН/g, /ЦИКЛОП/g, /ЦИЛИНДР/g, /ЦИЛИНДРИК/g, /ЦИЛИНДРСИМОН/g,
	/ЦИРК/g, /ЦИРКУЛЬ/g, /ЦИСТЕРНА/g, /ЦИТРУС/g, /НЬЮ-ЙОРК/g, /КОМПЬЮТЕР/g, /АНСАМБЛЬ/g,
	/АППЕЛЛЯЦИЯ/g, /АПЕЛЬСИН/g, /БАНДЕРОЛЬ/g, /БИЛЬЯРД/g, /БУЛЬДОЗЕР/g, /ФРАНЦИЯ/g, /ЭКСПОРТ/g, /АЭРОПОРТ/g,
	/ВЬЕТНАМ/g, /КОНФЕРЕНЦИЯ/g,

	// last attempt
	/октябрь/ig, /сентябрь/ig, /ноябрь/ig, /декабрь/ig,	/январь/ig,	/февраль/ig, /апрель/ig, /июнь/ig,
	/июль/ig, /бюджет/ig, /режиссёр/ig, /исҳоқ/ig, /нуқтаий/ig, /цезий/ig, /цейтнот/ig, /целлофан/ig,
	/целлюлоза/ig, /цельсий/ig, /цемент/ig, /цензор/ig, /цензура/ig, /центнер/ig, /цех/ig, /цеце/ig,
	/цивилизация/ig, /цикл/ig, /циклон/ig, /циклоп/ig, /цилиндр/ig, /цилиндрик/ig, /цилиндрсимон/ig,
	/цирк/ig, /циркуль/ig, /цистерна/ig, /цитрус/ig, /нью-йорк/ig, /компьютер/ig, /ансамбль/ig,
	/аппелляция/ig, /апельсин/ig, /бандероль/ig, /бильярд/ig, /бульдозер/ig, /франция/ig, /экспорт/ig, /аэропорт/g,
	/вьетнам/g, /конференция/g,
];

// exceptional words ltn
const exceptionalWordsLtn = [

	// first
	/Oktabr/g, /Sentabr/g, /Noyabr/g, /Dekabr/g, /Yanvar/g, /Fevral/g, /Aprel/g, /Iyun/g, /Iyul/g, /Budjet/g,
	/Rejissor/g, /Is'hoq/g, /Nuqtayi/g, /Seziy/g, /Seytnot/g, /Sellofan/g, /Sellyuloza/g, /Selsiy/g, /Sement/g,
	/Senzor/g, /Senzura/g, /Sentner/g, /Sex/g, /Setse/g, /Sivilizatsiya/g, /Sikl/g, /Siklon/g, /Siklop/g,
	/Silindr/g, /Silindrik/g, /Silindrsimon/g, /Sirk/g, /Sirkul/g, /Sisterna/g, /Sitrus/g, /Nyu-York/g,
	/Kompyuter/g, /Ansambl/g, /Apellatsiya/g, /Apelsin/g, /Banderol/g, /Bilyard/g, /Buldozer/g, /Fransiya/g,
	/Eksport/g, /Aeroport/g, /Vetnam/g, /Konferensiya/g,

	// second
	/OKTABR/g, /SENTABR/g, /NOYABR/g, /DEKABR/g, /YANVAR/g, /FEVRAL/g, /APREL/g, /IYUN/g, /IYUL/g, /BUDJET/g,
	/REJISSOR/g, /IS'HOQ/g, /NUQTAYI/g, /SEZIY/g, /SEYTNOT/g, /SELLOFAN/g, /SELLYULOZA/g, /SELSIY/g, /SEMENT/g,
	/SENZOR/g, /SENZURA/g, /SENTNER/g, /SEX/g, /SETSE/g, /SIVILIZATSIYA/g, /SIKL/g, /SIKLON/g, /SIKLOP/g,
	/SILINDR/g, /SILINDRIK/g, /SILINDRSIMON/g, /SIRK/g, /SIRKUL/g, /SISTERNA/g, /SITRUS/g, /NYU-YORK/g,
	/KOMPYUTER/g, /ANSAMBL/g, /APELLATSIYA/g, /APELSIN/g, /BANDEROL/g, /BILYARD/g, /BULDOZER/g, /FRANSIYA/g,
	/EKSPORT/g, /AEROPORT/g, /VETNAM/g, /KONFERENSIYA/g,

	// last attempt
	/oktabr/ig, /sentabr/ig, /noyabr/ig, /dekabr/ig, /yanvar/ig, /fevral/ig, /aprel/ig, /iyun/ig, /iyul/ig,
	/budjet/ig, /rejissor/ig, /is'hoq/ig, /nuqtayi/ig, /seziy/ig, /seytnot/ig, /sellofan/ig, /sellyuloza/ig,
	/selsiy/ig, /sement/ig, /senzor/ig, /senzura/ig, /sentner/ig, /sex/ig, /setse/ig, /sivilizatsiya/ig, /sikl/ig,
	/siklon/ig, /siklop/ig, /silindr/ig, /silindrik/ig, /silindrsimon/ig, /sirk/ig, /sirkul/ig, /sisterna/ig,
	/sitrus/ig, /nyu-york/ig, /kompyuter/ig, /ansambl/g, /apellatsiya/ig, /apelsin/ig, /banderol/g, /bilyard/g,
	/buldozer/ig, /fransiya/g, /eksport/ig, /aeroport/g, /vetnam/g, /konferensiya/g,
];

// notice! replacee is not a legal english word
// but here it is justified:))
module.exports = {
	crl: {
		exceptionalWords: {
			replacee: exceptionalWordsCrl,
			replacement: exceptionalWordsLtn
		},
		phonemRules: {
			replacee: exceptionalPhonemasCrl[0],
			replacement: exceptionalPhonemasCrl[1]
		},
		alphabet: {
			replacee: crlAlphabet,
			replacement: ltnAlphabet
		}
	},
	ltn: {
		exceptionalWords: {
			replacee: exceptionalWordsLtn,
			replacement: exceptionalWordsCrl
		},
		phonemRules: {
			replacee: exceptionalPhonemasLtn[0],
			replacement: exceptionalPhonemasLtn[1]
		},
		alphabet: {
			replacee: ltnAlphabet,
			replacement: crlAlphabet
		}
	},
	urlPattern: urlPattern,
	forceKeepPattern: forceKeepPattern,
	forceTranslitPattern: forceTranslitPattern
};
