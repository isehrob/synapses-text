/**
 * Module implementing transliteration functionality
 *
 *
 * */

import Rules from './rules';

// extending String prototype to implement replace all functionality
String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};


const translitExceptionalWords = (exceptionalWords, inputText) => {
    return exceptionalWords.replacee.reduce((reduced, replacee, i) => {
        // source is the source text of the RegExp
        return reduced.replace(replacee, exceptionalWords.replacement[i].source);
    }, inputText);
}

const translitWithPhonemaRules = (rules, inputText) => {
    return rules.replacee.reduce((reduced, replacee, i) => {
        return reduced.replace(replacee, rules.replacement[i]);
    }, inputText);
}

const translitRest = (alphabet, inputText) => {
    return alphabet.replacee.reduce((reduced, replacee, i) => {
        reduced = reduced.replaceAll(replacee, alphabet.replacement[i]);
		return reduced.replaceAll(
			replacee.toUpperCase(),
			alphabet.replacement[i].toUpperCase()
		);
    }, inputText);
}

const keepLinks = (pattern, inputText) => {
    const links = inputText.match(pattern);
    return [inputText.replace(pattern, '$#$'), links];
}

const reapplyLinks = (links, inputText) => {
    return links.reduce(
        (reduced, link) => inputText.replace('$#$', link),
        inputText
    )
}

// get out of the 'ь' in the middle of the crl word
function clearMiddleSoftSymbol(text){
	return text.replace(/([а-яА-Я])ь([а-яА-Я])/ig, '$1$2');
}

// utility function helps to know the input lang char
export function whichChar (text) {
	const alphabet = Rules.ltn.alphabet.replacee;
	const wholeSize = text.length;
	let ltnChars = 0;
	let thisChar = null;
	for(let i = 0; i < wholeSize; i++){
		if(alphabet.indexOf(text[i]) !== -1){
			ltnChars++;
		}
	}
	if(ltnChars == 0){
		thisChar = 'crl';
	}else{
		const percent = ltnChars / wholeSize;
		if(percent > 0.5){
			thisChar = 'ltn';
		}else{
			thisChar = 'crl';
		}
	}
	return thisChar;
}

export default (inputChar, inputText) => {

	const exceptionalWords = Rules[inputChar].exceptionalWords;
	const rules = Rules[inputChar].phonemRules;
	const alphabet = Rules[inputChar].alphabet;

    const result = keepLinks(
        Rules.urlPattern,
        inputText
    )
    // translit exceptional words
    let outputText = translitExceptionalWords(
        exceptionalWords,
        result[0]
    );

    outputText = translitWithPhonemaRules(
        rules,
        outputText
    );
	// translit with phonema rules
	// translit all the rest
	outputText = translitRest(
        alphabet, outputText
    );

    if(result[1]) {
        outputText = reapplyLinks(result[1], outputText);
    }
    if(inputChar === 'ltn') outputText = clearMiddleSoftSymbol(outputText);
    return outputText;
}
