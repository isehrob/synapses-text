// answer-page

import React from 'react';
import ReactDOM from 'react-dom';
import SynapsesTextEditor from './index.js';

// loading static files
import '../public/css/bootstrap.min.css';
import '../public/css/Draft.min.css';
import '../public/css/fonts.css';
import '../public/fonts/flaticons/flaticon.css';
import '../public/css/index.css';
import '../public/css/richEditor.css';



const App = (props) => {
	return (
		<div className="row">
			<div className="col-main">
				<SynapsesTextEditor />
			</div>
			<div className="clearfix"></div>
		</div>
	);
}


ReactDOM.render(
	<App />,
	document.getElementById('app-root')
);
