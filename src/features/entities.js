
import React from 'react';


export const Link = (props) => {
    const {contentState, entityKey} = props;
    const {url} = contentState.getEntity(entityKey).getData();
    return (
        <a href={url} style={{textDecoration: 'underline', color: 'blue'}}>
            {props.children}
        </a>
    );
};

// link decoration
export function findLinkEntities(contentBlock, callback, contentState) {
    contentBlock.findEntityRanges(
        (character) => {
            const entityKey = character.getEntity();
            return (
                entityKey !== null &&
                contentState.getEntity(entityKey).getType() === 'LINK'
               );
            }, callback
    );
}

export const TestEntity = (props) => {
    return (<h4>{props.data.koko}</h4>);
}

const Image = (props) => {
    return <img src={props.src} alt={props.alt} />;
};

const Video = (props) => {
    return <video controls src={props.src} style={{width: "100%", padding: "20px 50px"}}/>;
};


export const AtomicComponentManager = (props) => {
    const { contentState, block} = props;
    const entity = contentState.getEntity(block.getEntityAt(0));
    const entityData = entity.getData();
    const type = entity.getType();

    let AtomicComponent = null;

    if (type === 'image') AtomicComponent = <Image src={entityData.src} alt="" />;
    else if (type === 'video') AtomicComponent = <Video src={entityData.src} />;
    else if (type === 'TEST') AtomicComponent = <TestEntity data={entityData} />;

    return AtomicComponent;
}
